import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default function LoginScreen() {
  const [username, setUsername] = useState("username");
  const [password, setPassword] = useState("password");

  return (
    <View style={styles.container}>
      <Image style={styles.imageLogo} source={require("../assets/Logo.png")} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  imageLogo: {
    height: 100,
    width: 100,
    marginBottom: 20,
  },
});
